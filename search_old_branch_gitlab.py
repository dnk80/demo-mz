import json
from datetime import datetime
import requests

url_subgp = 'https:// /api/v4/groups/'
token = input('Enter Gitlab Personal-Token: ')
headers = {'PRIVATE-TOKEN': token}
group_id = str('47774') # Задаем корневой проект
id_lst = [int(group_id)]

# Питон съедает сам себя ) Рекурсивная выборка ID подгрупп любой глубины вложенности до тех пор пока есть группа
def subgroup_check(group_id):
    data = json.loads(requests.get(url_subgp + group_id + '/subgroups', headers=headers).text)
    for i in range(0, len(data)):
        id_lst.append(data[i]['id']) # Формируем список подгрупп и дополняем его
        if data[i]['id'] != '' :
            group_id = data[i]['id']
            subgroup_check(str(group_id))

subgroup_check(group_id)

for c in range(0, len(id_lst)):
    data = json.loads(requests.get(url_subgp + str(id_lst[c]), headers=headers).text)
    if not data['projects']:
        print()
    else:
        for k in range(0, len(data['projects'])): # Проходимся по всем проектам в группе из id_lst[c]
            proj_id = dict(data['projects'][k])['id'] # Получаем с группы project ID
            if "/os-" in json.loads(requests.get('https:// com/api/v4/projects/' + str(proj_id), headers=headers).text)['web_url'] :
                print('ALOHA',json.loads(requests.get('https:// com/api/v4/projects/' + str(proj_id), headers=headers).text)['web_url'])
            elif "/depric" in json.loads(requests.get('https:// com/api/v4/projects/' + str(proj_id), headers=headers).text)['web_url'] :
                print('ALOHA',json.loads(requests.get('https:// com/api/v4/projects/' + str(proj_id), headers=headers).text)['web_url'])
            else:
                branches = json.loads(requests.get('https:// com/api/v4/projects/' + str(proj_id) + '/repository/branches', headers=headers).text)

                for b in range(0, len(branches)):
                    if branches[b]['protected'] == False: # Если проектед флаг не взведен
                        date = (dict(branches[b]['commit'])['created_at'].split('T')[0]).split('-') # Парсим дату последнего мержа
                        datenow = datetime(datetime.now().year, datetime.now().month, datetime.now().day) #Get actual date
                        yy = date[0]
                        mm = date[1::][0].strip('0') # Убираем передний (первый нолик) в разряде числа месяца
                        dd = date[2::][0].strip('0') # Убираем передний (первый нолик) в разряде числа дня
                        create_date = datetime(int(yy), int(mm), int(dd))
                        try:
                            delta_time = int(str(datenow - create_date).split(',')[0].split(' ')[0]) #Если лочиться дата или еще не сформирована (заглушка надо дебажить что лочиться)
                        except ValueError:
                            print('')
                        if delta_time > 30 : #Если бранч старше 30 дней
                            print('Last activity:', str(datenow - create_date).split(',')[0])
                            print("web_url:", json.loads(requests.get('https:// com/api/v4/projects/' + str(proj_id), headers=headers).text)['web_url'])
                            print('Branch name:', branches[b]['name'])
                            print('Created:',      dict(branches[b]['commit'])['created_at'])
                            print('Last commited:',dict(branches[b]['commit'])['committed_date'])
                            print('Author:',       dict(branches[b]['commit'])['author_name'])
                            print('Author mail:',  dict(branches[b]['commit'])['author_email'])
                            print('')
                    elif not branches[b]['protected']: #Если протектед не существует в принципе, в нем не существует ничего
                        print("web_url:", json.loads(requests.get('https:// com/api/v4/projects/' + str(proj_id), headers=headers).text)['web_url'])
                        print('Branch name:', branches[b]['name'])
                        print('Created:', dict(branches[b]['commit'])['created_at'])
                        print('Author:', dict(branches[b]['commit'])['author_name'])
                        print('Author mail:', dict(branches[b]['commit'])['author_email'])
                        print('')
